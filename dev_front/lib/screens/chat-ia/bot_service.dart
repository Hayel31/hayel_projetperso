// bot_service.dart
import 'package:http/http.dart' as http;
import 'dart:convert';

Future<String> getBotResponse(String message) async {
  var response = await http.post(
    Uri.parse('https://api.openai.com/v1/engines/davinci-codex/completions'),
    headers: {
      'Authorization': 'Bearer dfd7e02e70f846e09d54070066be3950',
      'Content-Type': 'application/json',
    },
    body: jsonEncode({
      'prompt': message,
      'max_tokens': 60,
    }),
  );

  if (response.statusCode == 200) {
    var data = jsonDecode(response.body);
    if (data['choices'] != null && data['choices'].isNotEmpty && data['choices'][0]['text'] != null) {
      return data['choices'][0]['text'].trim();
    } else {
      throw Exception('No text found in the response from the GPT-3 API');
    }
  } else {
    throw Exception('Failed to get response from the GPT-3 API');
  }
}
