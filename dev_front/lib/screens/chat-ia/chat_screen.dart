import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

class ChatScreen extends StatefulWidget {
  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  final _messagesController = ScrollController();
  final _messageController = TextEditingController();
  final _messages = <Map<String, dynamic>>[];

  Future<void> _sendMessage() async {
    final message = _messageController.text;
    _messageController.clear();

    if (message.isNotEmpty) {
      setState(() {
        _messages.add({'sender': 'User', 'message': message});
      });

      var url = Uri.parse('https://api.openai.com/v1/engines/davinci-codex/completions');
      var response = await http.post(url,
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer dfd7e02e70f846e09d54070066be3950', 
        },
        body: jsonEncode({
          'prompt': message,
          'max_tokens': 60,
        }),
      );
      var decoded = jsonDecode(response.body);
      var aiMessage = decoded['choices'][0]['text'].trim();

      setState(() {
        _messages.add({'sender': 'IA', 'message': aiMessage});
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Colors.black),
          onPressed: () => Navigator.of(context).pop(),
        ),
        title: Text('IA Assistance', style: TextStyle(color: Colors.black)),
        backgroundColor: Colors.white,
        elevation: 0,
      ),
      body: Column(
        children: <Widget>[
          Expanded(
            child: ListView.builder(
              controller: _messagesController,
              itemCount: _messages.length,
              itemBuilder: (context, index) {
                return ListTile(
                  leading: CircleAvatar(
                    child: Text(_messages[index]['sender'][0]),
                    backgroundColor: _messages[index]['sender'] == 'IA' ? Colors.blue : Colors.grey,
                  ),
                  title: Text(_messages[index]['sender'], style: TextStyle(fontWeight: FontWeight.bold, color: _messages[index]['sender'] == 'IA' ? Colors.blue : Colors.black)),
                  subtitle: Text(_messages[index]['message'], style: TextStyle(color: Colors.black54)),
                );
              },
            ),
          ),
          Container(
            padding: EdgeInsets.all(8.0),
            child: Row(
              children: <Widget>[
                Expanded(
                  child: TextField(
                    controller: _messageController,
                    decoration: InputDecoration(
                      labelText: 'Enter your message',
                      labelStyle: TextStyle(color: Colors.blue),
                      focusedBorder: UnderlineInputBorder(borderSide: BorderSide(color: Colors.blue)),
                    ),
                  ),
                ),
                IconButton(
                  icon: Icon(Icons.send, color: Colors.blue),
                  onPressed: _sendMessage,
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
