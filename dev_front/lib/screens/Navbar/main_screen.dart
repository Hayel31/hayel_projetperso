import 'package:flutter/material.dart';
import 'package:shop_ui_with_ai/screens/Navbar/feed_page.dart';
import 'package:shop_ui_with_ai/screens/Navbar/map_page.dart';
import 'package:shop_ui_with_ai/screens/Navbar/profile_page.dart';
import 'package:shop_ui_with_ai/screens/Navbar/search_page.dart';
import 'package:shop_ui_with_ai/screens/Navbar/community_page.dart';

class MainScreen extends StatefulWidget {
  MainScreen({Key? key}) : super(key: key);

  @override
  _MainScreenState createState() => _MainScreenState();
}

class _MainScreenState extends State<MainScreen> {
  int _currentIndex = 2; // Set initial index to feed_page
  
  List<Widget> _children = [
    SearchPage(),
    CommunityPage(),
    FeedPage(),
    MapPage(),
    ProfilePage(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: _children[_currentIndex],
      bottomNavigationBar: BottomAppBar(
        shape: AutomaticNotchedShape(
          RoundedRectangleBorder(),
          StadiumBorder(side: BorderSide()),
        ),
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              buildIconButton(Icons.search, 0),
              buildIconButton(Icons.group, 1),
              SizedBox(width: 50), // Leaving space for the floating action button
              buildIconButton(Icons.map, 3),
              buildIconButton(Icons.person, 4),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.home, size: 30),
        onPressed: () => setState(() => _currentIndex = 2),
        backgroundColor: _currentIndex == 2 ? Colors.blue : Colors.grey,
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
    );
  }

  Widget buildIconButton(IconData icon, int index) {
    return IconButton(
      iconSize: 25,
      icon: Transform.scale(
        scale: _currentIndex == index ? 1.4 : 1.0,
        child: Icon(icon),
      ),
      onPressed: () {
        setState(() {
          _currentIndex = index;
        });
      },
      color: _currentIndex == index ? Colors.blue : Colors.grey,
    );
  }
}
