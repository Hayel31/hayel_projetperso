import 'package:flutter/material.dart';
import 'package:share/share.dart';
import 'package:shop_ui_with_ai/screens/chat-ia/bot_service.dart';
import 'package:shop_ui_with_ai/screens/chat-ia/chat_screen.dart';

class FeedPage extends StatefulWidget {
  @override
  _FeedPageState createState() => _FeedPageState();
}

class _FeedPageState extends State<FeedPage> {
  List<bool> liked = List.generate(6, (index) => false);
  List<List<String>> comments = List.generate(6, (index) => []);
  List<bool> showAllComments = List.generate(6, (index) => false);
  List<TextEditingController> controllers = List.generate(6, (index) => TextEditingController());

  // Ajouter cette méthode pour ouvrir le chatbot
  void _openChatBot() {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (context) => ChatScreen(),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Feed', style: TextStyle(fontFamily: 'RobotoMono', fontWeight: FontWeight.bold, fontSize: 30.0)),
        centerTitle: true,
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topLeft,
              end: Alignment.bottomRight,
              colors: <Color>[
                Theme.of(context).primaryColor,
                Theme.of(context).colorScheme.secondary,
              ],
            ),
          ),
        ),
      ),
      body: Stack( // Modifier ListView.builder en Stack
        children: [
          ListView.builder(
            itemCount: 6,
            itemBuilder: (BuildContext context, int index) {
              return Card(
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(15.0),
                ),
                elevation: 8.0,
                child: Column(
                  children: [
                    ListTile(
                      leading: CircleAvatar(
                        backgroundImage: NetworkImage('https://www.psg.fr/media/237552/gettyimages-1450106798.jpg?quality=60&width=1000&bgcolor=ffffff'),
                        radius: 25,
                      ),
                      title: Text('Post Title', style: TextStyle(fontFamily: 'RobotoMono', fontWeight: FontWeight.bold)),
                      subtitle: Text('Post Description', style: TextStyle(fontFamily: 'RobotoMono')),
                      trailing: Row(
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          IconButton(
                            icon: Icon(
                              Icons.favorite,
                              color: liked[index] ? Colors.red : Colors.grey,
                            ),
                            onPressed: () {
                              setState(() {
                                liked[index] = !liked[index];
                              });
                            },
                          ),
                          IconButton(
                            icon: Icon(
                              Icons.share,
                              color: Colors.grey,
                            ),
                            onPressed: () {
                              Share.share('Check out this post: Post URL'); 
                            },
                          ),
                        ],
                      ),
                    ),
                    ClipRRect(
                      borderRadius: BorderRadius.circular(15.0),
                      child: Image.network('https://www.psg.fr/media/237552/gettyimages-1450106798.jpg?quality=60&width=1000&bgcolor=ffffff'), 
                    ),
                    ListTile(
                      title: TextField(
                        controller: controllers[index],
                        decoration: InputDecoration(
                          labelText: 'Add a comment...',
                          suffixIcon: IconButton(
                            icon: Icon(Icons.send),
                            onPressed: () {
                              setState(() {
                                comments[index].add(controllers[index].text);
                                controllers[index].clear();
                              });
                            },
                          ),
                        ),
                      ),
                    ),
                    if (comments[index].isNotEmpty)
                      ...comments[index].sublist(0, showAllComments[index] ? comments[index].length : 1)
                          .map(
                            (comment) => ListTile(
                              title: Text(comment),
                            ),
                          )
                          .toList(),
                    if (comments[index].length > 1)
                      TextButton(
                      onPressed: () {
                        setState(() {
                          showAllComments[index] = !showAllComments[index];
                        });
                      },
                      child: Text(showAllComments[index] ? 'Show less' : 'Show more'),
                    ),
                  ],
                ),
              );
            },
          ),
          // Ajouter le bouton d'assistance IA ici:
          Positioned(
              right: 20,
              bottom: 80,
              child: FloatingActionButton(
                child: CircleAvatar(
                  backgroundImage: NetworkImage('https://media.cdnandroid.com/item_images/1404468/imagen-chat-gpt-open-chat-ai-bot-0ori.jpg'), 
                  radius: 100, // Remplacez par la taille souhaitée pour le cercle
                ),
                backgroundColor: Colors.blue, // Remplacez par la couleur souhaitée
		            mini: true,
                onPressed: _openChatBot,
              ),
            ),


        ],
      ),
    );
  }
}

