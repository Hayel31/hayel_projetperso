import 'package:flutter/material.dart';
import 'package:shop_ui_with_ai/constants.dart';
import 'package:shop_ui_with_ai/screens/auth/auth_screen.dart'; // Import added
import 'package:shop_ui_with_ai/screens/splash/splash_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'The Flutter Way',
      // ... (reste du code)

      // Ajouter SignUpScreen dans les routes si nécessaire
      routes: {
        '/signup': (context) => SignUpScreen(),
      },

      home: SplashScreen(title: 'Votre titre'),
    );
  }
}
